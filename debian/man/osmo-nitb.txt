NAME
  osmo-nitb - Osmocom GSM Network in the Box

SYNOPSIS
  osmo-nitb [options]

DESCRIPTION
 This is the Network-in-a-Box version of OpenBSC. It has all the GSM network
 components bundled together. When using osmocom-nitb, there is no need for a
 Mobile Switching Center (MSC) which is needed when using osmocom-bsc.

 Typically you would have to create a configuration file and start
 osmo-nitb with the "-c" option. There are example configuration files in:
 
   /usr/share/doc/osmocom-nitb/examples/osmo-nitb


OPTIONS
  -h,--help                  This text.
  -d option,--debug=DRLL:DCC:DMM:DRR:DRSL:DNM    Enable debugging.
  -D,--daemonize             Fork the process into a background daemon.
  -c,--config-file <filename>     The config file to use.

  -s,--disable-color

  -l,--database <db-name>    The database to use.
  -a,--authorize-everyone    Authorize every new subscriber. Dangerous!
  -T,--timestamp             Prefix every log line with a timestamp.
  -V,--version               Print the version of OpenBSC.
  -P,--rtp-proxy             Enable the RTP Proxy code inside OpenBSC.
  -e,--log-level <number>    Set a global loglevel.
  -m,--mncc-sock             Disable built-in MNCC handler and offer socket.
  -C,--no-dbcounter          Disable regular syncing of counters to database.
  -r,--rf-ctl <NAME>         A unix domain socket to listen for cmds.


SEE ALSO
  osmo-bsc(1)

AUTHOR
  This manual page was written by Ruben Undheim <ruben.undheim@gmail.com> for the Debian project (and may be used by others).



