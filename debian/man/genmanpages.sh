#!/bin/bash


txt2man -d "${CHANGELOG_DATE}" -t OSMO-BSC               -s 1 osmo-bsc.txt             > osmo-bsc.1
txt2man -d "${CHANGELOG_DATE}" -t OSMO-BSC_MGCP          -s 1 osmo-bsc_mgcp.txt        > osmo-bsc_mgcp.1
txt2man -d "${CHANGELOG_DATE}" -t OSMO-GBPROXY           -s 1 osmo-gbproxy.txt         > osmo-gbproxy.1
txt2man -d "${CHANGELOG_DATE}" -t OSMO-SGSN              -s 1 osmo-sgsn.txt            > osmo-sgsn.1
txt2man -d "${CHANGELOG_DATE}" -t IPACCESS-CONFIG        -s 1 ipaccess-config.txt      > ipaccess-config.1
txt2man -d "${CHANGELOG_DATE}" -t IPACCESS-FIND          -s 1 ipaccess-find.txt        > ipaccess-find.1
txt2man -d "${CHANGELOG_DATE}" -t IPACCESS-PROXY         -s 1 ipaccess-proxy.txt       > ipaccess-proxy.1
txt2man -d "${CHANGELOG_DATE}" -t BS11_CONFIG            -s 1 bs11_config.txt          > bs11_config.1
txt2man -d "${CHANGELOG_DATE}" -t ISDNSYNC               -s 1 isdnsync.txt             > isdnsync.1
txt2man -d "${CHANGELOG_DATE}" -t OSMO_NITB              -s 1 osmo-nitb.txt            > osmo-nitb.1
txt2man -d "${CHANGELOG_DATE}" -t OSMO-BSC_NAT           -s 1 osmo-bsc_nat.txt         > osmo-bsc_nat.1
