NAME
  osmo-bsc - Osmocom BSC

SYNOPSIS
  osmo-bsc [options]

DESCRIPTION
 This is the BSC-only version of OpenBSC. It requires a Mobile Switching Center
 (MSC) to operate.
 
 You might rather prefer to use osmocom-nitb which is considered a
 "GSM Network-in-a-Box" and does not depend on a MSC.

 Typically you start osmo-bsc with the -c option specifying where to find the
 configuration file. There are example configuration files in:

   /usr/share/doc/osmocom-bsc/examples/osmo-bsc

OPTIONS
  -h,--help                         this text
  -D,--daemonize                    Fork the process into a background daemon
  -d option,--debug=DRLL:DCC:DMM:DRR:DRSL:DNM       enable debugging

  -s,--disable-color

  -T,--timestamp                    Print a timestamp in the debug output.
  -c,--config-file <filename>       The config file to use.
  -l,--local=IP                     The local address of the MGCP.
  -e,--log-level <number>           Set a global loglevel.
  -r,--rf-ctl <NAME>                A unix domain socket to listen for cmds.
  -t,--testmode                     A special mode to provoke failures at the MSC.


SEE ALSO
  osmo-nitb(1)

AUTHOR
  This manual page was written by Ruben Undheim <ruben.undheim@gmail.com> for the Debian project (and may be used by others).



